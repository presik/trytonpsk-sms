# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import ModelSQL, ModelView, fields

try:
    from twilio.rest import Client
except ImportError:
    pass


__all__ = ['SmsServer', 'SmsTemplate']


class SmsServer(ModelSQL, ModelView):
    "Sms Server"
    __name__ = 'sms.server'
    name = fields.Char('Name', required=True)
    account_sid = fields.Char('Account SID', required=True)
    auth_token = fields.Char('Authentication Token')
    from_phone = fields.Char('From Phone', required=True)
    supplier = fields.Selection([
            ('twilio', 'Twilio'),
            ('clickatell', 'Clickatell'),
            ], 'Supplier', required=True)
    default = fields.Boolean('Default')

    @classmethod
    def __setup__(cls):
        super(SmsServer, cls).__setup__()
        cls._buttons.update({
            'sms_test': {},
        })

    @staticmethod
    def default_default():
        return True

    def send_message(self, phone, msg):
        if self.supplier == 'twilio':
            client = Client(self.account_sid, self.auth_token)
            if not phone.startswith('+'):
                phone = '+57' + phone.replace(" ", "")
            message = client.messages.create(
                body=msg,
                from_=self.from_phone,
                to=phone,
            )
            return message


class SmsTemplate(ModelSQL, ModelView):
    "Sms Template"
    __name__ = 'sms.template'
    name = fields.Char('Name', required=True)
    active = fields.Boolean('Active')
    server = fields.Many2One('sms.server', 'Server', required=True)
    code = fields.Char('Code')
    content = fields.Text('Content')

    @classmethod
    def __setup__(cls):
        super(SmsTemplate, cls).__setup__()
