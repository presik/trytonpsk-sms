# This file is part sms module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from .sms import SmsServer, SmsTemplate


def register():
    Pool.register(
        SmsServer,
        SmsTemplate,
        module='sms', type_='model')
